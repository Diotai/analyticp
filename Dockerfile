FROM manuelfgr/tensorflow-dlib:v1
MAINTAINER Manuel Felipe Garcia Rincon "manuel.garcia@globai.co"
ADD analyticp /home/analyticp
RUN pip install -r /home/analyticp/requirements.txt
WORKDIR /home/analyticp/src/
CMD ["python", "main.py"]
