import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class sendEmail:
    def __init__(self, toaddr, fromaddr, passw, categoria, cond, cantidad):
        categoria = str(categoria)
        if categoria == '1':
            categoria = 'persona'
        elif categoria == '2':
            categoria = 'bicicleta'
        elif categoria == '44':
            categoria = 'botella'
        elif categoria == '4':
            categoria = 'motocicleta'
        elif categoria == '3':
            categoria = 'carro'
        self.categoria = str(categoria)
        self.cantidad = str(cantidad)
        self.toaddr = str(toaddr)
        self.cond = str(cond)
        self.fromaddr = fromaddr  # "email@gmail.com"
        self.passw = passw

    def send(self, image):

        msg = MIMEMultipart()
        msg['From'] = self.fromaddr
        msg['To'] = self.toaddr
        msg['Subject'] = "Alerta conteo en poligono"
        if self.cond == "=":
            self.cond = "igual"
        elif self.cond == ">":
            self.cond = "mayor"
        body = "La cantidad de " + self.categoria + \
            "s es " + self.cond + " a: " + str(self.cantidad)
        # msg.attach(MIMEText(body, 'plain'))
        image_src = "data:image/jpeg;base64," + image
        html = """<p>""" + body + """</p><img src=""" + image_src + """>"""
        part2 = MIMEText(html, 'html')
        msg.attach(part2)
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(self.fromaddr, self.passw)
        text = msg.as_string()
        server.sendmail(self.fromaddr, self.toaddr, text)
        server.quit()
