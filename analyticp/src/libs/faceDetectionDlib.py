import cv2
import imutils
import numpy as np


def detect_faces_dlib(net, detPresTresh, frame):
    # frame = imutils.resize(frame, width=400)

    # if the frame dimensions are None, grab them
    (H, W) = frame.shape[:2]

    # construct a blob from the frame, pass it through the network,
    # obtain our output predictions, and initialize the list of
    # bounding box rectangles
    blob = cv2.dnn.blobFromImage(frame, 1.0, (W, H), (104.0, 177.0, 123.0))
    net.setInput(blob)
    detections = net.forward()
    rects = []

    # loop over the detections
    for i in range(0, detections.shape[2]):
        # filter out weak detections by ensuring the predicted
        # probability is greater than a minimum threshold
        if detections[0, 0, i, 2] > detPresTresh:
            # compute the (x, y)-coordinates of the bounding box for
            # the object, then update the bounding box rectangles list
            box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])
            (startX, startY, endX, endY) = box.astype("int")
            # cv2.rectangle(frame, (startX, startY), (endX, endY), (0, 0, 255), 2)
            rects.append((startX, startY, endX, endY))
    # print('detecciones en la funcion daceDetection_dlib:', rects)
    return rects
