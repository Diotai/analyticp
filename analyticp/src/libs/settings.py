from socketIO_client_nexus import SocketIO, LoggingNamespace
import time
import os
import confSettings


def settings(*args):
    global ips
    global validar
    global pl
    global device
    global ip
    global port
    global user
    global password
    global barrera

    print("Menasje recibido:", args[0])

    # ------------------------------INICIO VALIDACION DATOS------------------------
    if args[0]['key'] == "validar":
        ip_status, pl = confSettings.dataValid(args[0])
        message = "orange"
        socketIO.emit('state', message)
        time.sleep(3)
        if ip_status == 1:

            device = args[0]['messages']['device']
            ip = args[0]['messages']['ip']
            port = args[0]['messages']['port']
            user = args[0]['messages']['user']
            password = args[0]['messages']['password']

            validar = 1
            message = "green"
            socketIO.emit('state', message)
            print("informacion validada")
        else:
            print("revisar ip")
            message = "red"
            socketIO.emit('state', message)
    # ------------------------------FINAL VALIDACION DATOS------------------------

    # ------------------------------INICIO ENVIO DE IMAGEN------------------------
    elif args[0]['key'] == "take":
        if validar == 1:

            image = confSettings.takePic_b64toS(args[0], pl)
            print(image)
            message = '{"image": true, "buffer": "' + image + '"}'
            socketIO.emit('image', message)
            print("foto Enviada")
        else:
            print("No a validado la informacion")
    # ------------------------------FINAL ENVIO DE IMAGEN------------------------

    # ------------------------------INICIO RECEPCION DE BARRERA------------------------
    elif args[0]['key'] == "barrera":
        barrera = args[0]['messages']['barrera']
        validar = 0
        confSettings.stopS(device, pl)
        message = "blue"
        socketIO.emit('state', message)
        print("configuracion de barrera exitosa")
        print(args[0]['messages']['barrera'])
        print(ip, port, user, password)
        x1 = args[0]['messages']['barrera']['x1']
        y1 = args[0]['messages']['barrera']['y1']
        x2 = args[0]['messages']['barrera']['x2']
        y2 = args[0]['messages']['barrera']['y2']
        c1 = str(args[0]['messages']['category'][0])
        c2 = str(args[0]['messages']['category'][1])
        c3 = str(args[0]['messages']['category'][2])
        c4 = str(args[0]['messages']['category'][3])
        c5 = str(args[0]['messages']['category'][4])
        if args[0]['messages']['tipo'] == "0":
            os.system("python3 main_object_detection.py --iot yes --key " +
                      device + " --ip " + ip + " --port " + port + " --user " + user + " --password " + password + " --x1 " + x1 + " --y1 " + y1 + " --x2 " + x2 + " --y2 " + y2 + " --c1 " + c1 + " --c2 " + c2 + " --c3 " + c3 + " --c4 " + c4 + " --c5 " + c5 + " --ips " + ips)
        else:
            os.system("python3 main_object_counting.py --iot yes --key " +
                      device + " --ip " + ip + " --port " + port + " --user " + user + " --password " + password + " --x1 " + x1 + " --y1 " + y1 + " --x2 " + x2 + " --y2 " + y2 + " --c1 " + c1 + " --c2 " + c2 + " --c3 " + c3 + " --c4 " + c4 + " --c5 " + c5 + " --ips " + ips)
    # ------------------------------INICIO RECEPCION DE BARRERA------------------------


pl = ""
ip = ""
port = ""
user = ""
password = ""
barrera = ""
ips = "192.168.1.111"
validar = 0
flag = 0

socketIO = SocketIO(ips, 4000, LoggingNamespace)

print("conectado")
while True:
    socketIO.on('settings', settings)
    socketIO.wait(seconds=0.0005)
