from xml.etree import ElementTree as ET
import numpy as np
# ---------------------------------------------------------------------
# ------------------ INICIO EXTRACCION DE GTHS EN XML -----------------
# ---------------------------------------------------------------------
dom = ET.parse('frame117.xml')

objeto = dom.findall('object')
gth_coor = np.array([[]])
'''print(np.shape(objeto))
print(objeto[0][1].text)
print(objeto[1][1].text)
print(objeto[2][1].text)
print(objeto[3][1].text)
print(objeto[4][1].text)
print(objeto[5][1].text)
print(objeto[6][1].text)'''


if len(objeto) > 0:

    for coordenadas in objeto:

        if int(coordenadas[1].text) == 0:

            if int(coordenadas[9][3][0].text) < int(coordenadas[9][1][0].text):
                Xstart = int(coordenadas[9][3][0].text)
                Xend = int(coordenadas[9][1][0].text)
            else:
                Xstart = int(coordenadas[9][1][0].text)
                Xend = int(coordenadas[9][3][0].text)

            if int(coordenadas[9][3][1].text) < int(coordenadas[9][1][1].text):
                Ystart = int(coordenadas[9][3][1].text)
                Yend = int(coordenadas[9][1][1].text)
            else:
                Ystart = int(coordenadas[9][1][1].text)
                Yend = int(coordenadas[9][3][1].text)

            if gth_coor.shape[1] < 1:
                gth_coor = np.append(gth_coor, [(Xstart, Ystart, Xend, Yend)], axis=1)
            else:
                gth_coor = np.append(gth_coor, [(Xstart, Ystart, Xend, Yend)], axis=0)

else:
    gth_coor = np.array([[]])

# ---------------------------------------------------------------------
# -------------------- FIN EXTRACCION DE GTHS EN XML ------------------
# ---------------------------------------------------------------------
