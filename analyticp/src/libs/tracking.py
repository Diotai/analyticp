import numpy as np
# import math
import cv2
# import time
import utilidades


class tracking:
    def __init__(self, barrera, maxDistCentroide, numFrameDelete):
        self.dX = 0
        self.dY = 0
        self.counter = 0
        self.direction = ""
        self.barrera = barrera
        self.maxDistCetroide = maxDistCentroide
        self.numFrameDelete = numFrameDelete

        self.objects_tracker = []  # arreglo para almacenar los objetos de seguimientos que se van creando
        self.centroides = []  # arreglo para almacenar los centroide de los BBs entregados por el detector
        self.centroidesMemory = []  # arreglo para almacenar los centroides del frame anterior
        self.deletedMemory = []
        self.centroides_ejeX = []
        self.borrar_centroides_ejeX = []
        self.centroide_actual = []
        self.cola = []
        self.new_bbox_actual = []
        self.borrar_object_tracker = []
        self.borrar_centroides = []
        self.borrar_centroide_actual = []
        self.borrar_cola = []
        self.borrar_new_bbox_actual = []
        self.borrar_centroide_lejano = []
        self.bandera = 0
        self.total_de_carros = 0
        self.carMemoryInicial = 0
        self.carMemory = 0  # La cantidad de carros que habian en frame-1
        self.posicionMaxCentroide_ejeX_memory = 0
        self.valorMaxCentroide_ejeX_memory = 0
        self.distancia_eu = 0
        self.carros_actuales = 0
        self.contador = 0

    def inicializarVariables(self):
        self.dX = 0
        self.dY = 0
        self.objects_tracker = []
        self.centroides = []
        self.centroidesMemory = []
        self.deletedMemory = []
        self.centroides_ejeX = []
        self.centroide_actual = []
        self.cola = []
        self.new_bbox_actual = []
        self.borrar_object_tracker = []
        self.borrar_centroides = []
        self.borrar_centroide_actual = []
        self.borrar_cola = []
        self.borrar_new_bbox_actual = []
        self.borrar_centroide_lejano = []
        self.bandera = 0
        self.total_de_carros = 0
        self.carMemoryInicial = 0
        self.carMemory = 0
        self.posicionMaxCentroide_ejeX_memory = 0
        self.valorMaxCentroide_ejeX_memory = 0
        self.distancia_eu = 0
        self.carros_actuales = 0
        self.contador = 0

    def inicializacionDeSeguimiento(self, BBs, clone, clone2):

        if self.bandera == 0:  # es el primer frame del video que se esta procesando?
            posicion = 0
            for BB in BBs:  # recorre cada uno de las coordenadas BBs encontradas por el detector

                w = BB[2] - BB[0]  # se calcula el ancho del BB
                h = BB[3] - BB[1]  # se calcula el alto del BB
                # se dibuja de color azul sobre clone2 el BB
                cv2.rectangle(clone2, (BB[0], BB[1]), (BB[2], BB[3]), (255, 0, 0), 4)

                # se calcula el centroide del BB
                centroide_BB = int(BB[0] + (float(w) / float(2))
                                   ), int(BB[1] + (float(h) / float(2)))
                # el centroide del BB esta dentro de la region de interes? (el BB sigue sin pasar la barrera de deeteccion?)
                if centroide_BB[0] > self.barrera[0] and centroide_BB[0] < self.barrera[2] and centroide_BB[1] > self.barrera[1] and centroide_BB[1] < self.barrera[3]:
                    # if centroide_BB[0] > self.barrera:
                    # print("centroide_BB[0]= " + str(centroide_BB[0]))
                    # se arma las coordenadas de inicializacion para un nuevo objeto de tracking
                    BB_inicializacion = (BB[0], BB[1], w, h)
                    # se almacena centroide_BB en un arreglo llamdo centroides
                    self.centroides.append(centroide_BB)
                    # se almacena las cooredenadas del eje X de los centroides en un arreglo llamado centroides_ejeX
                    self.centroides_ejeX.append(centroide_BB[0])

                    self.new_bbox_actual.append((BB))
                    # se crea y se almacena en un arreglo llamado objects_tracker un objeto de seguimietno
                    self.objects_tracker.append(cv2.TrackerTLD_create())
                    self.objects_tracker[posicion].init(clone, BB_inicializacion)  # se inicializa
                    posicion += 1
            self.bandera = 1

            # se calcula en cual posicion del arreglo centroides_ejeX se encuentra su maximo valor y esta posicion es guardada posicionMaxCentroide_ejeX_memory
            # print("np.shape(self.centroides_ejeX: " + str(np.shape(self.centroides_ejeX)))
            self.posicionMaxCentroide_ejeX_memory = self.centroides_ejeX.index(
                max(self.centroides_ejeX))
            # se guarda en valorMaxCentroide_ejeX_memory el maximo valor del arreglo centroides_ejeX
            self.valorMaxCentroide_ejeX_memory = self.centroides[self.posicionMaxCentroide_ejeX_memory][0]
            self.centroidesMemory = self.centroides_ejeX[:]

            return clone2
        else:  # if self.bandera == 0 :

            for BB in BBs:

                cv2.rectangle(clone2, (BB[0], BB[1]), (BB[2], BB[3]), (255, 0, 0), 2)
                no_bbox = 0
                # print(BB)
                for new_bbox in self.new_bbox_actual:
                    if utilidades.bb_intersection_over_union(new_bbox, BB) < 0.05:
                        no_bbox += 1

                if no_bbox == len(self.new_bbox_actual):

                    w = BB[2] - BB[0]

                    h = BB[3] - BB[1]
                    centroide_BB = int(BB[0] + (float(w) / float(2))), int(
                        BB[1] + (float(h) / float(2)))
                    if centroide_BB[0] > self.barrera[0] and centroide_BB[0] < self.barrera[2] and centroide_BB[1] > self.barrera[1] and centroide_BB[1] < self.barrera[3]:
                        # if centroide_BB[0] > self.barrera:
                        posicion2 = len(self.objects_tracker)

                        self.objects_tracker.append(cv2.TrackerTLD_create())

                        self.centroides.append(
                            (int(BB[0] + (float(w) / float(2))), int(BB[1] + (float(h) / float(2)))))
                        self.centroides_ejeX.append(int(BB[0] + (float(w) / float(2))))

                        BB_inicializacion = (BB[0], BB[1], w, h)

                        self.objects_tracker[posicion2].init(clone, BB_inicializacion)

                        self.new_bbox_actual.append((BB))
                        cv2.rectangle(clone2, (int(BB[0]), int(BB[1])),
                                      (int(BB[2]), int(BB[3])), (0, 255, 255), 4)

            return clone2

    def actualizarSeguimiento(self, clone, clone2):

        self.centroide_actual = []
        self.new_bbox_actual = []
        # self.cola = []
        if self.bandera == 1:
            # bandera_if = 0
            delete = 0
            # print(np.shape(self.objects_tracker))
            for object_t in self.objects_tracker:

                self.distancia_eu = 0
                delete += 1

                nbbox = []
                # t4 = time.time()
                ok, nbbox = object_t.update(clone)
                # t5 = time.time()
                # print("tiempo de procesamiento TRACKING: " + str(t5 - t4))
                if ok is True:
                    # print ("true")

                    filasF = int(nbbox[1]) + int(nbbox[3])
                    columnasF = int(nbbox[0]) + int(nbbox[2])
                    filasI = int(nbbox[1])
                    columnasI = int(nbbox[0])

                    if (filasI <= (np.shape(clone)[0] - 1)) and (filasF <= (np.shape(clone)[0] - 1)) and (columnasI <= (np.shape(clone)[1] - 1)) and (columnasF <= (np.shape(clone)[1] - 1)):

                        self.centroide_actual.append(
                            (int(nbbox[0] + (float(nbbox[2]) / float(2))), int(nbbox[1] + (float(nbbox[3]) / float(2)))))
                        self.centroides_ejeX[delete -
                                             1] = int(nbbox[0] + (float(nbbox[2]) / float(2)))
                        distancia_eu = pow((pow((self.centroides[delete - 1][0] - self.centroide_actual[delete - 1][0]), 2) + pow(
                            (self.centroides[delete - 1][1] - self.centroide_actual[delete - 1][1]), 2)), 0.5)

                        self.new_bbox_actual.append((int(nbbox[0]), int(nbbox[1]), int(
                            nbbox[0] + nbbox[2]), int(nbbox[1] + nbbox[3])))

                        self.centroides[delete - 1] = self.centroide_actual[delete - 1]

                        # print("dist_u: " + str(distancia_eu))
                        if self.centroide_actual[delete - 1][0] < self.barrera[0] and self.centroide_actual[delete - 1][0] > self.barrera[2] and self.centroide_actual[delete - 1][1] < self.barrera[1] and self.centroide_actual[delete - 1] > self.barrera[3]:
                            # if self.centroide_actual[delete - 1][0] <= self.barrera:
                            self.borrar_centroides.append(delete - 1)
                            # print("borrar_centroide 1")
                            self.borrar_centroides_ejeX.append(delete - 1)
                            self.borrar_centroide_lejano.append(delete - 1)
                            self.borrar_object_tracker.append(delete - 1)
                            self.borrar_centroide_actual.append(delete - 1)
                            self.borrar_new_bbox_actual.append(delete - 1)

                        elif distancia_eu > self.maxDistCetroide:

                            self.borrar_centroides.append(delete - 1)
                            # print("borrar_centroide 2")
                            self.borrar_centroides_ejeX.append(delete - 1)
                            self.borrar_centroide_lejano.append(delete - 1)
                            self.borrar_object_tracker.append(delete - 1)
                            self.borrar_centroide_actual.append(delete - 1)
                            # self.borrar_cola.append(delete - 1)
                            self.borrar_new_bbox_actual.append(delete - 1)

                        else:
                            self.centroides[delete - 1] = self.centroide_actual[delete - 1]
                            self.centroides_ejeX[delete -
                                                 1] = self.centroide_actual[delete - 1][0]

                    else:
                        self.borrar_centroides.append(delete - 1)
                        # print("borrar_centroide 4")
                        self.borrar_centroides_ejeX.append(delete - 1)
                        self.borrar_centroide_lejano.append(0)
                        self.borrar_object_tracker.append(delete - 1)
                        self.centroide_actual.append((0, 0))
                        self.new_bbox_actual.append((0, 0, 0, 0))
                        self.borrar_new_bbox_actual.append(delete - 1)
                else:

                    self.borrar_centroides.append(delete - 1)
                    # print("borrar_centroide 5")
                    self.borrar_centroides_ejeX.append(delete - 1)
                    self.borrar_centroide_lejano.append(0)
                    self.borrar_object_tracker.append(delete - 1)
                    self.centroide_actual.append((0, 0))
                    self.new_bbox_actual.append((0, 0, 0, 0))

            for borrar in sorted(self.borrar_centroides, reverse=True):
                self.centroides.pop(borrar)
                self.centroides_ejeX.pop(borrar)
                self.objects_tracker.pop(borrar)

            for borrar in sorted(self.borrar_centroide_actual, reverse=True):
                self.centroide_actual.pop(borrar)

            for borrar in sorted(self.borrar_new_bbox_actual, reverse=True):
                self.new_bbox_actual.pop(borrar)

            # print("np.shape(self.centroides): " + str(np.shape(self.centroide_actual)))
            # print("np.shape(self.centroide_actual): " + str(np.shape(self.centroides)))
            # print("np.shape(self.centroides_ejeX): " + str(np.shape(self.centroides_ejeX)))
            # print("np.shape(self.objects_tracker): " + str(np.shape(self.objects_tracker)))

            self.borrar_centroides = []
            self.borrar_centroides_ejeX = []
            self.borrar_centroide_actual = []
            self.borrar_centroide_lejano = []
            self.borrar_object_tracker = []
            self.borrar_new_bbox_actual = []
            for new_bbox in self.new_bbox_actual:
                # print("Linea 180 new_bbox: " + str(new_bbox))
                cv2.rectangle(clone2, (new_bbox[0], new_bbox[1]),
                              (new_bbox[2], new_bbox[3]), (0, 255, 0), 4)
    # def inicializacionDeSeguimiento(self, BBs, clone, clone2):
    # parametros:
    # BBs: (Bounding Box)coordenadas de los vehiculos que encontro el detector
    # clone: un clon de la imagen origanl del frame para hacer el seguimiento de los vehiculos
    # clone2: otro clon de la imagen original para dibujar los textos y las etiquetas

    def conteo(self):
        guardar = 0
        borrar = []
        carRecordado = 0

        self.carros_actuales = len(self.objects_tracker)

        if self.carros_actuales == self.carMemory:  # la cantidad de carros que encontro es igual a la del frame anterior?

            self.contador += 1

            if self.contador > self.numFrameDelete:
                self.deletedMemory = []
                self.contador = 0

            if np.shape(self.centroides)[0] > 0:
                # la posicion del centroide mas alejado cambio con respecto al frame anterior?
                if self.posicionMaxCentroide_ejeX_memory != self.centroides[self.centroides_ejeX.index(max(self.centroides_ejeX))][0]:
                    # hay una diferencia mayor a 100px entre el centroide (eje x) mas alejado del frame anterioro con el del frame actual?
                    if abs(self.valorMaxCentroide_ejeX_memory - self.centroides[self.centroides_ejeX.index(max(self.centroides_ejeX))][0]) > 100:
                        self.total_de_carros += 1  # aumente en uno el  numero de vehiculos totales
                        # print("XXXXXXXXXXXXXXXXXXXXXXXXXX___mas 1___XXXXXXXXXXXXXXXXXXXXXXXXXX")
                # se actualiza posicionMaxCentroide_ejeX_memory con el valor actual
                self.posicionMaxCentroide_ejeX_memory = self.centroides_ejeX.index(
                    max(self.centroides_ejeX))
                # se actualiza valorMaxCentroide_ejeX_memorym con el valor actual
                self.valorMaxCentroide_ejeX_memory = self.centroides[self.posicionMaxCentroide_ejeX_memory][0]

        if len(self.objects_tracker) > self.carMemory:
            self.contador = 0

            if self.carMemoryInicial > 0:
                self.carMemory = self.carMemoryInicial
                self.carMemoryInicial = 0

            if np.shape(self.centroides)[0] > 0:

                if len(self.deletedMemory) > 0:
                    for deleteM in self.deletedMemory:
                        carRecordado_delete = 0
                        for centroide_x in self.centroides_ejeX:
                            if abs(deleteM - centroide_x) < 50:
                                carRecordado += 1
                                carRecordado_delete += 1
                        if carRecordado_delete > 0:
                            borrar.append(self.deletedMemory.index(deleteM))
                    for borre in sorted(borrar, reverse=True):
                        self.deletedMemory.pop(borre)
                if (len(self.objects_tracker) - self.carMemory - carRecordado) >= 0:
                    self.total_de_carros += (len(self.objects_tracker) -
                                             self.carMemory - carRecordado)
                    self.carMemory = len(self.objects_tracker)

                    self.posicionMaxCentroide_ejeX_memory = self.centroides_ejeX.index(
                        max(self.centroides_ejeX))
                    self.valorMaxCentroide_ejeX_memory = self.centroides[self.posicionMaxCentroide_ejeX_memory][0]
                    # self.deletedMemory = []

        if len(self.objects_tracker) < self.carMemory:
            self.contador = 0

            for centroideM in self.centroidesMemory:
                for centroide_x in self.centroides_ejeX:

                    if abs(centroideM - centroide_x) > 50:
                        guardar += 1
                if guardar == len(self.centroides):
                    self.deletedMemory.append(centroideM)

            if np.shape(self.centroides)[0] > 0:
                # print("******" + str(np.shape(self.centroides)))
                # print("&&&&&&" + str(self.centroides))
                self.carMemory = len(self.objects_tracker)
                # print("------" + str(self.centroides_ejeX))
                centroideMaxMemory = self.centroides_ejeX.index(max(self.centroides_ejeX))
                self.valorMaxCentroide_ejeX_memory = self.centroides[centroideMaxMemory][0]
            else:
                self.carMemory = 0  # len(self.objects_tracker)

        self.centroidesMemory = self.centroides_ejeX[:]

        return self.carros_actuales, self.total_de_carros

    def reset(self):
        self.objects_tracker = []
