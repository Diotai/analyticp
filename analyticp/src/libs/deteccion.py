import cv2
import numpy as np
from statistics import mode
from utils.datasets import get_labels
from utils.inference import detect_faces
from utils.inference import apply_offsets
from utils.inference import load_detection_model
from utils.preprocessor import preprocess_input
from keras.models import load_model
from libs.faceDetectionDlib import detect_faces_dlib


class deteccion:
    def __init__(self, nombre, cat, detPresTresh):
        self.detPresTresh = detPresTresh
        # lectura para con DLI
        self.net = cv2.dnn.readNetFromCaffe('libs/deploy.prototxt', 'libs/' + nombre)
        # parameters for loading data and images
        detection_model_path = '../trained_models/detection_models/haarcascade_frontalface_default.xml'
        emotion_model_path = '../trained_models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
        gender_model_path = '../trained_models/gender_models/simple_CNN.81-0.96.hdf5'
        self.emotion_labels = get_labels('fer2013')
        self.gender_labels = get_labels('imdb')

        # hyper-parameters for bounding boxes shape
        self.frame_window = 10
        self.gender_offsets = (30, 60)
        self.emotion_offsets = (20, 40)

        # loading models
        self.face_detection = load_detection_model(detection_model_path)
        self.emotion_classifier = load_model(emotion_model_path, compile=False)
        self.gender_classifier = load_model(gender_model_path, compile=False)

        # getting input model shapes for inference
        self.emotion_target_size = self.emotion_classifier.input_shape[1:3]
        self.gender_target_size = self.gender_classifier.input_shape[1:3]

        # starting lists for calculating modes
        self.gender_window = []
        self.emotion_window = []

    def detector(self, image, barrera):
        pop2 = []
        emotion = []
        gender = []
        if type(image) == str:
            image_np = cv2.imread(image)
        else:
            image_np = image

        gray_image = cv2.cvtColor(image_np, cv2.COLOR_BGR2GRAY)
        rgb_image = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
        # faces = detect_faces(self.face_detection, gray_image)
        faces = detect_faces_dlib(self.net, self.detPresTresh, image_np)
        # for face_coordinates in faces:
        #     (startX, startY, endX, endY) = face_coordinates
        #     cv2.rectangle(image_np, (startX, startY), (endX, endY), (50, 150, 0), 2)

        for face_coordinates in faces:

            x1, x2, y1, y2 = apply_offsets(face_coordinates, self.gender_offsets)
            rgb_face = rgb_image[y1:y2, x1:x2]

            x1, x2, y1, y2 = apply_offsets(face_coordinates, self.emotion_offsets)
            gray_face = gray_image[y1:y2, x1:x2]
            try:
                rgb_face = cv2.resize(rgb_face, (self.gender_target_size))
                gray_face = cv2.resize(gray_face, (self.emotion_target_size))
            except:
                continue
            gray_face = preprocess_input(gray_face, False)
            gray_face = np.expand_dims(gray_face, 0)
            gray_face = np.expand_dims(gray_face, -1)
            emotion_label_arg = np.argmax(self.emotion_classifier.predict(gray_face))
            emotion_text = self.emotion_labels[emotion_label_arg]
            self.emotion_window.append(emotion_text)

            rgb_face = np.expand_dims(rgb_face, 0)
            rgb_face = preprocess_input(rgb_face, False)
            gender_prediction = self.gender_classifier.predict(rgb_face)
            gender_label_arg = np.argmax(gender_prediction)
            gender_text = self.gender_labels[gender_label_arg]
            self.gender_window.append(gender_text)

            if len(self.gender_window) > self.frame_window:
                self.emotion_window.pop(0)
                self.gender_window.pop(0)
            try:
                emotion_mode = mode(self.emotion_window)
                gender_mode = mode(self.gender_window)
            except:
                continue

            pop2.append(np.array((face_coordinates[0], face_coordinates[1], face_coordinates[2], face_coordinates[3])))
            gender.append(np.array(gender_mode))
            emotion.append(np.array(emotion_mode))

        clone2 = image_np.copy()

        # barrera[0] = x1
        # barrera[1] = y1
        # barrera[2] = x2
        # barrera[3] = y2

        cv2.rectangle(clone2, (barrera[0], barrera[1]), (barrera[2], barrera[3]), (0, 0, 150), 2)

        return pop2, gender, emotion, clone2

    def detectorTracking(self, image, barrera, faces):
        pop2 = []
        emotion = []
        gender = []
        if type(image) == str:
            image_np = cv2.imread(image)
        else:
            image_np = image

        gray_image = cv2.cvtColor(image_np, cv2.COLOR_BGR2GRAY)
        rgb_image = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
        # faces = detect_faces(self.face_detection, gray_image)

        for face_coordinates in faces:

            x1, x2, y1, y2 = apply_offsets(face_coordinates, self.gender_offsets)
            rgb_face = rgb_image[y1:y2, x1:x2]

            x1, x2, y1, y2 = apply_offsets(face_coordinates, self.emotion_offsets)
            gray_face = gray_image[y1:y2, x1:x2]
            try:
                rgb_face = cv2.resize(rgb_face, (self.gender_target_size))
                gray_face = cv2.resize(gray_face, (self.emotion_target_size))
            except:
                continue
            gray_face = preprocess_input(gray_face, False)
            gray_face = np.expand_dims(gray_face, 0)
            gray_face = np.expand_dims(gray_face, -1)
            emotion_label_arg = np.argmax(self.emotion_classifier.predict(gray_face))
            emotion_text = self.emotion_labels[emotion_label_arg]
            self.emotion_window.append(emotion_text)

            rgb_face = np.expand_dims(rgb_face, 0)
            rgb_face = preprocess_input(rgb_face, False)
            gender_prediction = self.gender_classifier.predict(rgb_face)
            gender_label_arg = np.argmax(gender_prediction)
            gender_text = self.gender_labels[gender_label_arg]
            self.gender_window.append(gender_text)

            if len(self.gender_window) > self.frame_window:
                self.emotion_window.pop(0)
                self.gender_window.pop(0)
            try:
                emotion_mode = mode(self.emotion_window)
                gender_mode = mode(self.gender_window)
            except:
                continue

            pop2.append(np.array((face_coordinates[0], face_coordinates[1], face_coordinates[2], face_coordinates[3])))
            gender.append(np.array(gender_mode))
            emotion.append(np.array(emotion_mode))

        clone2 = image_np.copy()

        # barrera[0] = x1
        # barrera[1] = y1
        # barrera[2] = x2
        # barrera[3] = y2

        cv2.rectangle(clone2, (barrera[0], barrera[1]), (barrera[2], barrera[3]), (0, 0, 150), 2)

        return gender, emotion, clone2
