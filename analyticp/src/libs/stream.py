import vlc
import ctypes
import time
import sys
import cv2
import numpy
from PIL import Image
import threading

# dir1 = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov"
# dir2 = "rtsp://192.168.1.119:554/StreamingSetting?version=1.0&sessionID=91665612&action=getRTSPStream&ChannelID=1&ChannelName=Channel1&EncoderPort=0"
# dir3 = "sample.mp4"
# dir4 = "mario.mp3"
# dir7 = ""

VIDEOWIDTH = 640
VIDEOHEIGHT = 368

# size in bytes when RV32
size = VIDEOWIDTH * VIDEOHEIGHT * 4
# allocate buffer
buf = (ctypes.c_ubyte * size)()
# get pointer to buffer
buf_p = ctypes.cast(buf, ctypes.c_void_p)

# global frame (or actually displayed frame) counter
# global framenr
framenr = 0
# vlc.CallbackDecorators.VideoLockCb is incorrect
CorrectVideoLockCb = ctypes.CFUNCTYPE(
    ctypes.c_void_p, ctypes.c_void_p, ctypes.POINTER(ctypes.c_void_p))


@CorrectVideoLockCb
def _lockcb(opaque, planes):
    planes[0] = buf_p


@vlc.CallbackDecorators.VideoDisplayCb
def _display(opaque, picture):
    global framenr
    global opencvImage
    global opencvImage2
    img = Image.frombuffer(
        "RGBA", (VIDEOWIDTH, VIDEOHEIGHT), buf, "raw", "RGBA", 0, 1)
    opencvImage = numpy.array(img)
    opencvImage2 = opencvImage[:, :, 0:3]
    framenr += 1


opencvImage = numpy.zeros((VIDEOHEIGHT, VIDEOWIDTH, 4))
opencvImage2 = numpy.zeros((VIDEOHEIGHT, VIDEOWIDTH, 3))
# print(opencvImage)
# vlc.libvlc_video_set_callbacks(pl, _lockcb, None, _display, None)
# pl.video_set_format("RV32", VIDEOWIDTH, VIDEOHEIGHT, VIDEOWIDTH * 4)

# pl.play()


# time.sleep(5)
# while(1):

#     print(numpy.shape(opencvImage))
#     cv2.imshow('Output', opencvImage)
#     if cv2.waitKey(100) & 0xFF == ord('q'):
#         break
# # time.sleep(10)
