from socketIO_client_nexus import SocketIO, LoggingNamespace
import imutils
from libs.deteccion import deteccion as det
from libs.centroidtracker import CentroidTracker
from libs.sendEmail import sendEmail
from copy import deepcopy
from os import environ as env
import numpy as np
import utilidades
import threading
# import argparse
from utils.inference import draw_text
import requests
import base64
import time
import cv2
import dlib

flag_gT = 0
time_gt = 0


def sendDataMail(whoMail, frameSL, http_s, ipsMail, portMail, endpoint, sendE, toaddr, dataSL, condicion):
    if whoMail == "ia":
        sendE.send(base64.b64encode(
            cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8'))
    elif whoMail == "bk":
        try:
            re = requests.post(http_s + "://" + ipsMail + ":" + portMail + "/" + endpoint, json={'image': base64.b64encode(
                cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8'), 'condicion': condicion, "cantidad": dataSL, "cliente": toaddr})
            if (int(re.status_code) != 200):
                whoMail = "ia"
                return True
        except(requests.exceptions.Timeout, requests.exceptions.ConnectionError, requests.exceptions.HTTPError):
            whoMail = "ia"
            return False


def getFrame(*args):
    global trama
    global flag_gT
    global time_gt
    global flag_pcss
    threadLock.acquire()
    trama = deepcopy(args)
    threadLock.release()
    getF.acquire()
    flag_pcss = True
    getF.release()
    flag_gT += 1
    print("recibi frame:", flag_gT, 'len(trama):', len(trama))
    temp = time.time()
    # print('tiempo recibi frame:', (temp-time_gt))
    time_gt = temp


def escuchar():
    global socketIO

    while(1):
        print('escuchando...')
        socketIO.on('frame', getFrame)
        socketIO.wait(seconds=0.001)


def sendEm():
    global socketIO
    global frameS
    global dataS
    global timeF
    global emailF
    global cond
    global stateMail
    global whoMail
    global ipsMail
    global portMail
    global http_s
    global cantidad
    stateMail = False
    while(1):
        lockDataMail.acquire()
        frameSL = deepcopy(frameS)
        dataSL = deepcopy(dataS)
        lockDataMail.release()

        if cond == "=":
            if dataSL == cantidad and stateMail == False:
                stateMail = sendDataMail(whoMail, frameSL, http_s, ipsMail, portMail, endpoint, sendE, toaddr, dataSL, "igual")

            if dataSL != cantidad:
                stateMail = False

        elif cond == ">" and stateMail == False:
            stateMail = sendDataMail(whoMail, frameSL, http_s, ipsMail, portMail, endpoint, sendE, toaddr, dataSL, "mayor")

            if dataSL < cantidad:
                stateMail = False


flaf_sd = 0
time_sd = 0


def sendData():
    global socketIO
    global frameS
    global dataS
    global dataSMemory
    global timeF
    global flaf_sd
    global time_sd
    global dataiDS

    while(1):
        lockTime.acquire()
        timeFL = timeF
        lockTime.release()
        # print('timeFL:', timeFL)
        if timeFL == 1:
            lockData.acquire()
            frameSL = deepcopy(frameS)
            dataiDSL = deepcopy(dataiDS)
            dataGenderL = deepcopy(dataGender)
            dataEmotionL = deepcopy(dataEmotion)
            lockData.release()
            # print('len(frameSL):', len(frameSL))
            flaf_sd += 1
            if len(frameSL) > 0:
                print('entre a enviar')
                socketIO.emit('data', {'idApp': id, 'type': 'analyticp', 'id': dataiDSL, 'gender': dataGenderL, 'emotion': dataEmotionL, 'frame': base64.b64encode(
                    cv2.imencode('.jpg', frameSL)[1]).decode('UTF-8')})
                print('envie frame:', flaf_sd)
            else:
                print('NO envie frame:', flaf_sd, 'porque el frame estaba vacio')
            lockTime.acquire()
            timeF = 0
            lockTime.release()
            temp = time.time()
            # print('tiempo envie frame', flaf_sd, ':', (temp-time_sd))
            time_sd = temp


# --------------- INICIO ARGUMENTOS ---------------
# ap = argparse.ArgumentParser()
# ap.add_argument("-ipStream", "--ipStream", required=True, help="server")
# ap.add_argument("-portSteram", "--portSteram", required=True, help="server")
# ap.add_argument("-x1", "--x1", required=True, help="x1")
# ap.add_argument("-y1", "--y1", required=True, help="y1")
# ap.add_argument("-x2", "--x2", required=True, help="x2")
# ap.add_argument("-y2", "--y2", required=True, help="y2")
# ap.add_argument("-category", "--category", required=True, help="c1")
# ap.add_argument("-id", "--id", required=True, help="id app")
# ap.add_argument("-toaddr", "--toaddr", required=True, help="toaddr")
# ap.add_argument("-cond", "--cond", required=True, help="cond alert")
# ap.add_argument("-cantidad", "--cantidad", required=True, help="cantidad alert")

# args = vars(ap.parse_args())
ips = env["ipStream"]
port = env["portSteram"]
x1 = int(env["x1"])
y1 = int(env["y1"])
x2 = int(env["x2"])
y2 = int(env["y2"])
id = str(env["id"])
toaddr = str(env["toaddr"])
cond = str(env["cond"])
cantidad = str(env["cantidad"])
categoria = str(env["category"])
maxDisappeared = int(env["maxDisappeared"])
maxDistance = int(env["maxDistance"])
totalFrames = int(env["totalFrames"])
passFRames = int(env["passFRames"])
detPresTresh = float(env["detPresTresh"])  # 6.0
nmsTresh = float(env["nmsTresh"])  # 0.75
drawBB = str(env["drawBB"])
modelName = str(env["modelName"])
fromaddr = str(env["fromaddr"])
passw = str(env["passw"])
whoMail = str(env["whoMail"])
endpoint = str(env["endpoint"])
ipsMail = env["ipsMail"]
portMail = env["portMail"]
production = env["production"]
http_s = env["http_s"]
if cantidad != 'none':
    cantidad = int(cantidad)

cat = []
if categoria == "person":
    cat.append(1)  # person

elif categoria == "bicycle":
    cat.append(2)  # cuchillo

elif categoria == "bottle":
    cat.append(44)  # botella

elif categoria == "car":
    cat.append(3)  # carro

elif categoria == "motorcycle":
    cat.append(4)  # moto

print("category")
# --------------- FINAL ARGUMENTOS ---------------
# --------------- INICIO VARIABLES ---------------
trackers = []
nombre = modelName  # "ssd_mobilenet_v1_coco_11_06_2017"
barrera = (x1, y1, x2, y2)
trama = []
frameS = []
dataS = 0
dataSMemory = -1
timeF = 0
emailF = False
flag_pcss = False
flag_process = 0
time_ps = 0
stateMail = True
mitrama = []
# --------------- FINAL VARIABLES ---------------
# --------------- INICIO DEFINICION DE OBJETOS ---------------
deteccion = det(nombre, cat, detPresTresh)
sendE = sendEmail(toaddr, fromaddr, passw, cat[0], cond, cantidad),
socketIO = SocketIO(http_s + "://" + ips, port, LoggingNamespace)
ct = CentroidTracker(maxDisappeared, maxDistance)
if production == "false":
    cap = cv2.VideoCapture(0)
# --------------- FINAL DEFINICION DE OBJETOS ---------------

# --------------- INICIO DEFINICION THREADS ---------------
threadLock = threading.Lock()
lockData = threading.Lock()
lockDataMail = threading.Lock()
lockTime = threading.Lock()
getF = threading.Lock()

hilo1 = threading.Thread(target=escuchar)
hilo2 = threading.Thread(target=sendData)
if cond != 'none':
    hilo3 = threading.Thread(target=sendEm)
    hilo3.start()
# --------------- INICIO DEFINICION THREADS ---------------
if production == "true":
    hilo1.start()
hilo2.start()


object_tracker = []
bandera = 0
total_de_carros = 0
while (1):
    # print('procesando...')
    if production == "true":
        tcopy = time.time()
        threadLock.acquire()
        mitrama = deepcopy(trama)
        threadLock.release()
        getF.acquire()
        flag_pcss_local = flag_pcss
        getF.release()
        tcopy2 = time.time()
    else:
        mitrama.append(0)
        flag_pcss_local = True
    # print('time copy:', tcopy2 - tcopy)
    # print('len(mitrama):', len(mitrama), 'and', 'flag_pcss_local:', flag_pcss_local)
    if len(mitrama) > 0 and flag_pcss_local == True:
        if production == "true":
            tgimg = time.time()
            barray = base64.b64decode(mitrama[0])
            jpg_as_np = np.frombuffer(barray, dtype=np.uint8)
            image = cv2.imdecode(jpg_as_np, flags=1)
        else:
            ret, image = cap.read()
            image = imutils.resize(image, width=400)
            # print('len image:', np.shape(image))
        gender_mode2 = []
        emotion_mode2 = []
        emotion_mode_send = 0
        gender_mode_send = 0
        if totalFrames % passFRames == 0 or total_de_carros == 0 or len(trackers) != total_de_carros:
            ta = time.time()
            trackers = []
            pop2, gender, emotion, clone2 = deteccion.detector(
                image, barrera)
            # print('len clone2: N1', np.shape(clone2))
            # print('******', pop2, gender, emotion)
            #
            pop3, gender2, emotion2 = utilidades.non_max_suppression_fast(np.array(pop2), np.array(gender), np.array(emotion), nmsTresh)  # los BB que tengan un % de overlap > al threshold seran eliminados
            # pop3 = pop2
            # print('pop3:', pop3, 'len:', len(pop3))

            for BB, gender_mode, emotion_mode in zip(pop3, gender2, emotion2):  # recorre cada uno de las coordenadas BBs encontradas por el detector
                w = BB[2] - BB[0]  # se calcula el ancho del BB
                h = BB[3] - BB[1]  # se calcula el alto del BB
                face_coordinates = (BB[0], BB[1], w, h)
                # BB_inicializacion = (BB[0], BB[1], w, h)
                # tracker = cv2.TrackerCSRT_create()
                # tracker.init(image, BB_inicializacion)

                tracker = dlib.correlation_tracker()
                rect = dlib.rectangle(BB[0], BB[1], BB[2], BB[3])
                tracker.start_track(image, rect)

                # add the tracker to our list of trackers so we can
                # utilize it during skip frames
                trackers.append(tracker)
                # se dibuja de color azul sobre clone2 el BB
                if gender_mode == 'man':
                    gender_mode = 'hombre'
                    gender_mode_send = 1
                elif gender_mode == 'woman':
                    gender_mode = 'mujer'
                    gender_mode_send = 2

                gender_mode2.append(gender_mode_send)

                if emotion_mode == 'sad':
                    emotion_mode = 'triste'
                    emotion_mode_send = 1
                elif emotion_mode == 'happy':
                    emotion_mode = 'feliz'
                    emotion_mode_send = 2
                elif emotion_mode == 'angry':
                    emotion_mode = 'rabia'
                    emotion_mode_send = 3
                elif emotion_mode == 'surprise':
                    emotion_mode = 'sorprendido'
                    emotion_mode_send = 4
                elif emotion_mode == 'fear':
                    emotion_mode = 'miedo'
                    emotion_mode_send = 5

                emotion_mode2.append(emotion_mode_send)
                if drawBB == 'true':
                    cv2.rectangle(clone2, (BB[0], BB[1]), (BB[2], BB[3]), (255, 0, 0), 4)

                    draw_text(face_coordinates, clone2, gender_mode,
                              (255, 0, 0), 0, -20, 1, 1)
                    draw_text(face_coordinates, clone2, emotion_mode,
                              (255, 0, 0), 0, -45, 1, 1)

            tb = time.time()
            # print('deteccion:', tb - ta)
        else:
            tc = time.time()
            pop3 = []
            clone2 = image.copy()
            # print('len clone2: N2', np.shape(clone2))
            i = 0
            delTacker = []
            for tracker in trackers:
                tracker.update(image)
                pos = tracker.get_position()
                # unpack the position object
                startX = int(pos.left())
                startY = int(pos.top())
                endX = int(pos.right())
                endY = int(pos.bottom())
                centX = int(startX + ((endX - startX) / 2))
                centY = int(startY + ((endY - startY) / 2))
                if centX > barrera[0] and centX < barrera[2] and centY > barrera[1] and centY < barrera[3]:
                    # add the bounding box coordinates to the rectangles list
                    pop3.append((startX, startY, endX, endY))
                    # if drawBB == 'true':
                    #     cv2.rectangle(clone2, (startX, startY), (endX, endY), (255, 0, 0), 4)
                else:
                    delTacker.append(i)
                i += 1
            for i in range((len(delTacker) - 1), -1, -1):
                index = delTacker[i]
                del trackers[index]
            cv2.rectangle(clone2, (barrera[0], barrera[1]), (barrera[2], barrera[3]), (0, 0, 150), 2)
            td = time.time()
            # print('tracking:', td - tc)
            gender, emotion, clone2 = deteccion.detectorTracking(image, barrera, pop3)
            for BB, gender_mode, emotion_mode in zip(pop3, gender2, emotion2):  # recorre cada uno de las coordenadas BBs encontradas por el detector
                w = BB[2] - BB[0]  # se calcula el ancho del BB
                h = BB[3] - BB[1]  # se calcula el alto del BB
                face_coordinates = (BB[0], BB[1], w, h)
                # BB_inicializacion = (BB[0], BB[1], w, h)
                # tracker = cv2.TrackerCSRT_create()
                # tracker.init(image, BB_inicializacion)

                # se dibuja de color azul sobre clone2 el BB
                if gender_mode == 'man':
                    gender_mode = 'hombre'
                    gender_mode_send = 1
                elif gender_mode == 'woman':
                    gender_mode = 'mujer'
                    gender_mode_send = 2

                gender_mode2.append(gender_mode_send)

                if emotion_mode == 'sad':
                    emotion_mode = 'triste'
                    emotion_mode_send = 1
                elif emotion_mode == 'happy':
                    emotion_mode = 'feliz'
                    emotion_mode_send = 2
                elif emotion_mode == 'angry':
                    emotion_mode = 'rabia'
                    emotion_mode_send = 3
                elif emotion_mode == 'surprise':
                    emotion_mode = 'sorprendido'
                    emotion_mode_send = 4
                elif emotion_mode == 'fear':
                    emotion_mode = 'miedo'
                    emotion_mode_send = 5

                emotion_mode2.append(emotion_mode_send)
                if drawBB == 'true':
                    cv2.rectangle(clone2, (BB[0], BB[1]), (BB[2], BB[3]), (0, 255, 0), 4)

                    draw_text(face_coordinates, clone2, gender_mode,
                              (255, 0, 0), 0, -20, 1, 1)
                    draw_text(face_coordinates, clone2, emotion_mode,
                              (255, 0, 0), 0, -45, 1, 1)
        totalFrames += 1
        objects, gen, emo = ct.update(pop3, gender_mode2, emotion_mode2)
        sendIDs = []
        sendemo = []
        sendgene = []
        for (objectID, genero) in gen.items():
            sendgene.append(genero)
        for (objectID, emocion) in emo.items():
            sendemo.append(emocion)
        for (objectID, centroid) in objects.items():
            sendIDs.append(objectID)
            # draw both the ID of the object and the centroid of the
            # object on the output frame
            # text = "ID {}".format(objectID)
            # print(text)
            # cv2.putText(clone2, text, (centroid[0] - 10, centroid[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv2.circle(clone2, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
        total_de_carros = len(objects.items())
        # print('ID_rev:', sendIDs, 'Gender_rev:', sendgene, 'Emotion_rev:', sendemo)
        # print(total_de_carros)
        if production == "false":
            posicion_print_numCarTotal = (5, 50)
            cv2.putText(clone2, "cantidad: " + str(total_de_carros), posicion_print_numCarTotal, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
            cv2.namedWindow('Output', cv2.WINDOW_NORMAL)
            cv2.resizeWindow('Output', 640, 360)
            cv2.imshow('Output', clone2)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        # print('ID:', sendIDs, 'Gender:', gender_mode2, 'Emotion', emotion_mode2)
        t2 = time.time()
        lockTime.acquire()
        timeF = 1
        lockTime.release()
        if production == "true":
            getF.acquire()
            flag_pcss = False
            getF.release()
        lockData.acquire()
        frameS = deepcopy(clone2)
        dataiDS = sendIDs
        dataGender = sendgene
        dataEmotion = sendemo
        lockData.release()

        lockDataMail.acquire()
        frameS = deepcopy(clone2)
        dataS = total_de_carros
        lockDataMail.release()
        flag_process += 1
        # print('frame:', flag_process, 'len:', len(clone2))
        # print('procese frame:', flag_process)
        # print('time decode image', tgimg2 - tgimg)
        # print('tiempo procees frame:', t2 - t1)
        # print(total_de_carros)
        temp = time.time()
        # print('tiempo procese', flag_process, ':', (temp-time_ps))
        time_ps = temp
